from django.contrib.auth.models import User
from django.http import Http404
from rest_framework.authentication import  TokenAuthentication
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from pro_shopping.models import order,Items,ordersItems
from pro_shopping.serializers import UserSerializer,ItemSerializer,orderSerializer,orderItems_Serializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class ItemViewset(viewsets.ModelViewSet):
    queryset = Items.objects.all()
    serializer_class = ItemSerializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]





class orderViewset(viewsets.ModelViewSet):
    queryset = order.objects.all()
    serializer_class = orderSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    # count=('-products_count')

    def list(self, request):
        # queryset = User.objects.all()
        order_obj =order.objects.filter(user_id = request.user)
        serializer = orderSerializer(order_obj, many=True)
        return Response(serializer.data)

    def create(self, request,  *args, **kwargs):
        data = request.data
        # userinstance=User.objects.get(id=2)
        print('user:',request.user)
        # print('username:',userinstance)
        new_order = order.objects.create(user_id=request.user,total=data['total'],status=data['status'],mode_of_payment=data['mode_of_payment'])
        new_order.save()
        orders_obj = order.objects.filter(user_id=request.user)
        print('orders_obj:',orders_obj)
        for product in data['products']:
            product_obj =Items.objects.get(title=product['title'])
            new_order.products.add(product_obj)
        serializer = orderSerializer(new_order)

        return Response(serializer.data)

    def retrive(self, request, pk=None):
        queryset = order.objects.filter(pk=pk,user_id=request.user, )
        if not queryset:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = orderSerializer(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        pass


    def partial_update(self, request, pk=None):
        orders= order.objects.get(user_id=request.user,pk=pk)
        data = request.data

        try:
            for product in data['products']:
                product_obj = Items.objects.get(title=product['title'])
                # products = Products.objects.get(title=data['title'])
                orders.products = product_obj
        except Exception:
            print('error',Exception)

        orders.total =data.get('total',orders.total)
        orders.status = data.get('status', orders.status)
        orders.mode_of_payment = data.get('mode_of_payment', orders.mode_of_payment)
        orders.save()

        serialized = orderSerializer(request.user, data=request.data, partial=True)
        return Response(serialized.data,status=status.HTTP_202_ACCEPTED)

    # def destroy(self, request, pk=None,*args, **kwargs):
    #     try:
    #         order=Orders.objects.get(user_id=request.user,pk=pk)
    #         instance = self.get_object(order)
    #         self.perform_destroy(instance)
    #     except Http404:
    #         pass
    #     return Response(status=status.HTTP_204_NO_CONT



class orderItems_Viewset(viewsets.ModelViewSet):
    queryset = ordersItems.objects.all()

    serializer_class = orderItems_Serializer
    authentication_classes =  [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    # def list(self, request):
    #     # queryset = User.objects.all()
    #     data = request.data
    #     order_obj =Orders_items.objects.filter(order_id= data['order_id'])
    #     serializer = Order_items_Serializer(order_obj, many=True)
    #     return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        data = request.data
        # user = User.objects.get(username=request.user)
        new_order_item = ordersItems.objects.get(user_id=request.user,product_id = request.Products,)
        pass