from django.db import models

# Create your models here.
from django.contrib.auth.models import User


# Create your models here.

class Items(models.Model):
    title=models.CharField(max_length=50)
    description = models.TextField()
    image=models.ImageField(upload_to='templates/images')
    price=models.FloatField(max_length=50)
    created_At = models.DateTimeField(auto_now_add=True)
    updated_At = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.title}'





class order(models.Model):
    PAYMENT_CHOICES = (
        ("Cash", "cash"),
        ("Paytm", "paytm"),
        ("Card", "mastercard"),
        ("Phonepay", "phonepay"),

    )
    STATUS_CHOICES=(
        ('New','new'),
        ('Paid','paid')
    )

    user_id=models.ForeignKey(User,on_delete=models.CASCADE)
    total =models.FloatField(max_length=50)
    products = models.ManyToManyField(Items)
    created_At = models.DateTimeField(auto_now_add=True)
    updated_At = models.DateTimeField(auto_now=True)
    status= models.CharField(max_length=20,choices=STATUS_CHOICES)
    mode_of_payment=models.CharField(max_length=20,choices=PAYMENT_CHOICES)
    # Quantity = models.IntegerField(default=0)
    # def total_items(self):
    #   orderitems = self.orderitems.all()
    # total = sum ([item.quantity for items in orderitems])
    # return total


    def __str__(self):
        return f"{self.id}"

class ordersItems(models.Model):
    user_id=models.ForeignKey(User,on_delete=models.CASCADE,default=None)

    # order_id=models.ForeignKey(Orders,on_delete=models.CASCADE)
    product_id=models.ForeignKey(Items,on_delete=models.CASCADE)
    Quantity=models.IntegerField()
    # price=models.IntegerField()
    # price = Products.objects.filter(product_id__price=Products.price)
    def price(self):
        return self.product_id.price

    def total_cost(self):
        print('product_id price:',self.product_id.price)
        cost=self.Quantity * self.product_id.price
        print('cost:',cost)
        return cost

    def __str__(self):
        return f"{self.product_id}"
    # def total(self):
    #     total=self.Quantity * self.price