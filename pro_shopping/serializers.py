from pro_shopping.models import order,Items,ordersItems
from rest_framework import serializers
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Items
        fields = [  'id',
                    'title' ,
                    'description' ,
                    'price' ,
                    'created_At' ,
                  'updated_At','image']



class orderSerializer(serializers.ModelSerializer):
    class Meta:
        model = order
        fields = [
            'id',
            'user_id',
            'total',
            'products',
            'created_At',
            'updated_At',
            'status',
            'mode_of_payment',

        ]
        depth =1
    # def update(self, instance, validated_data):
    #     order=validated_data.pop('Orders')
    #     orders=Orders.objects.create(**validated_data)
    #     # for order  in
    #     return orders

    def update(self, instance, validated_data):

        demo = order.objects.get(pk=instance.id)
        order.objects.filter(pk=instance.id) .update(**validated_data)
        return demo

class orderItems_Serializer(serializers.ModelSerializer):
    class Meta:
        model = ordersItems
        fields = [
            'id',
            'user_id',
            # 'order_id',
            'product_id',
            'Quantity',
            'price',
            'total_cost'
        ]
        depth = 2


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password']
        extra_kwargs = {'password': {'write_only': True, 'required': True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user

