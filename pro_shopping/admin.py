from django.contrib import admin

# Register your models here.

from pro_shopping.models import order,Items,ordersItems
# Register your models here.

class ItemAdmin(admin.ModelAdmin):
    list_display = ('id','title', 'description', 'image','price','created_At','updated_At')

class orderAdmin(admin.ModelAdmin):
    list_display = ('id','user_id', 'total','status','created_At','updated_At','mode_of_payment')

class ordersItems_Admin(admin.ModelAdmin):
    list_display = ('id','user_id', 'product_id', 'Quantity','price','total_cost')


admin.site.register(Items,ItemAdmin)
admin.site.register(order,orderAdmin)
admin.site.register(ordersItems,ordersItems_Admin)