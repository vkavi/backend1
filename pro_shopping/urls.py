from django.urls import path,include
from rest_framework import routers

from pro_shopping.rest_views import UserViewSet,ItemViewset,orderViewset,orderItems_Viewset
from rest_framework.authtoken.views import obtain_auth_token

router = routers.DefaultRouter()
router.register('account/register', UserViewSet)
router.register('product',ItemViewset)
router.register('orders',orderViewset,basename="Orders")
router.register('order_items',orderItems_Viewset)
# router.register('register', UserViewSet)

urlpatterns=[
    path('',include(router.urls)),
    path('account/login/',obtain_auth_token)
]
