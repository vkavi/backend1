from django.apps import AppConfig


class ProShoppingConfig(AppConfig):
    name = 'pro_shopping'
